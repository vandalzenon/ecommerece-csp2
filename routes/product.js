// [SECTION] DEPENDENCIES AND MODULES
let express = require('express')
let controller = require('./../controllers/product');
let {verify,verifyAdmin} = require('./../controllers/login');

// [SECTION] ROUTING COMPONENT
let route = express.Router();
// [SECTION] [GET]ROUTES
// fn[get all products]
route.get('/',(req, res) =>{
    controller.getProducts().then(result => {
        res.send(result);
    })
})
// fn[get product by Id]
route.get('/product/:productId',(req,res) =>{
    let id = req.params.productId
    controller.getOneProduct(id).then(result =>{
        res.send(result);
    })
})

route.get('/cart',verify,(req,res) =>{
    const userId = req.user.id;
 
    controller.getCart(userId).then(result => {
        res.send(result)
    })
})
// [SECTION] [PST]ROUTES
route.post('/create-product',verify ,verifyAdmin, (req,res) => {
    let data = req.body;
    controller.createProduct(data).then(result =>{
        res.send(result);
    })
})
// [SECTION] [PUT]ROUTES
// fn[archive product]
route.put('/:productId/archive', verify, verifyAdmin, (req,res)=>{
   let id = req.params.productId;
  
    controller.archiveProduct(id).then(result =>{
        res.send(result);
    })
})
// fn[restore product]
route.put('/:productId/restore', verify, verifyAdmin,  (req,res)=>{
    let id = req.params.productId;

    controller.restoreProduct(id).then(result =>{
        res.send(result);
    })
})

route.put('/product/:productId',verify,verifyAdmin,(req,res)=>{
    let id = req.params.productId;
    let data = req.body;
    if (data.name !== "" && data.description !== "" &&  data.price !== ""){
        controller.updateInformation(id,data).then(result =>{
            res.send(result);
        })
    }else{
        
        res.send({message:`some data seems to be missing`})
    }
  
})


route.put('/addCart/:prodID',verify,(req,res)=>{
    const data = req.params.prodID;
    const userId = req.user.id;
    controller.addCart(data, userId).then(result =>{
        res.send(result)
    })
})

route.put('/pullCart/:prodID',verify,(req,res)=>{
    const data = req.params.prodID;
    const userId = req.user.id;
    controller.pullCart(data, userId).then(result =>{
        res.send(result)
    })
})
// [SECTION] [DEL]ROUTES

route.delete('/product/:productId',(req,res) =>{
    let id = req.params.productId
    controller.deleteProduct(id).then(result =>{
        res.send(result);
    })
})

// [SECTION] AUTHORIZATION

// [SECTION] EXPORT ROUTES
module.exports = route;