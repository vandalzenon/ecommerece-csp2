// [SECTION] Dependencies and Modules
const express = require("express");
const mongoose = require("mongoose");
const dotenv = require("dotenv").config();
const cors = require("cors");
const userRoute = require("./routes/user");
const productRoute = require("./routes/product");
const loginController = require("./controllers/login");
const orderRoute = require('./routes/orders');



// [SECTION] Server Setup
let port = process.env.PORT;
let mongodB = process.env.CONNECTION_STRING;
let app = express();
app.use(express.json());
app.use(cors())

// [SECTION] Application Routes
app.use("/users", userRoute);
app.use("/products", productRoute);
app.use("/orders" ,orderRoute);
// [SECTION] Database connect
mongoose.connect(mongodB);
let dBStatus = mongoose.connection;
dBStatus.once("open", () => {
  console.log("\x1b[36m", `Connected to Database`);
});

// [SECTION] Gateway Response
app.listen(port, () => {
  console.log("\x1b[34m", `Server is running on port ${port}`);
});

// [SECTION] Default HTTP Response

app.get("/", (req, res) => {
  res.send(`Welcome to my Zenon's Ecommerce API server`);
});
app.post("/login", (req, res) => {
  let data = req.body;
  loginController.login(data).then((result) => {
    res.send(result);
  });
});
